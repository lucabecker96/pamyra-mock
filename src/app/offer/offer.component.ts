import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { VirtualTimeScheduler } from 'rxjs';
 
@Component({
  selector: 'app-offer',
  templateUrl: './offer.component.html',
  styleUrls: ['./offer.component.css']
})
export class OfferComponent implements OnInit {
  
  offerName: string;
  tarif: string; 
  price : number; 
  paymentMethod: string; 
  palette: string; 
  pickUpDate: string;
  deliveryDate: string; 

  constructor(private route : ActivatedRoute) {
    this.route= route
   }

  ngOnInit() {
    this.route.params.subscribe((params: any) => {
      let parameters = JSON.parse(params['offer'])[0]
      this.offerName = parameters.name
      this.tarif= parameters.tarif
      this.price = parameters.price
      this.paymentMethod = parameters.payment
      this.palette = parameters.palette
      this.pickUpDate = this.transformDateString(parameters.pickUpDate)
      this.deliveryDate = this.transformDateString(parameters.deliveryDate)
    });

  }
  transformDateString = (date : Date) :string => {
    let dateObj = new Date();
    let month = dateObj.getUTCMonth() + 1; //months from 1-12
    let day = dateObj.getUTCDate();    
    let newdate =    day + '.' +month ; 
    return newdate   
  }
}

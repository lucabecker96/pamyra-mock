import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {Routes, RouterModule} from '@angular/router'
import { AppComponent } from './app.component';
import { JourneyFormComponent } from './journey-form/journey-form.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { CitySearchComponent } from './city-search/city-search.component';
import { PackPieceComponent } from './pack-piece/pack-piece.component';
import { HttpClientModule } from '@angular/common/http'; 
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { HttpMockRequestInterceptor } from './http-mock-request-interceptor';
import { OfferComponent } from './offer/offer.component';

const routes: Routes = [
  { path: 'search', component: JourneyFormComponent },
  { path: 'result/:offer', component: OfferComponent},
  {path: '', component: JourneyFormComponent},

]

@NgModule({
  declarations: [
    AppComponent,
    JourneyFormComponent,
    CitySearchComponent,
    PackPieceComponent,
    OfferComponent
  ],
  imports: [
    HttpClientModule,
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(routes)
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpMockRequestInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

import { Component, OnInit, ChangeDetectionStrategy, Input } from '@angular/core';
import { FormGroup, FormBuilder, FormArray, Form } from '@angular/forms';


 
@Component({
  selector: 'app-pack-piece',
  templateUrl: './pack-piece.component.html',
  styleUrls: ['./pack-piece.component.css']
})
export class PackPieceComponent implements OnInit {
  @Input() index: number;
  @Input() parentForm: FormGroup
 // pack_1: FormGroup
  type: string;
  amount:  1;
  length: number;
  width: number;
  height: number;
  weight: number; 
  stackable: boolean ; 
  packPieceFormats = [
    {
      "category": "Paletten und Boxen",
      formats: [
        {"id": 0, "name" : "EUR-Palette", "length":120 , "width": 80, "height": 100,"weight": 100 },
        {"id": 1, "name" : "EUR-Palette mit Überhang", "length":120 , "width": 80, "height": 100,"weight": 100 },
        {"id": 2, "name" : "EUR-Gitterbox", "length":124 , "width": 84, "height": 97,"weight": 100 },
        {"id": 3, "name" : "Einweg-Palette", "length":120 , "width": 80, "height": 100,"weight": 100 },
        {"id": 4, "name" : "EUR6-Palette", "length":60 , "width": 80, "height": 100,"weight": 100 },
      ]
    },
    {
      "category": "Fahrzeuge",
      formats: [
        {"id": 5, "name" : "Fahrrad (im Karton)", "length":160 , "width": 20, "height": 80,"weight": 20 },
        {"id": 6, "name" : "E-Bike (im Karton)", "length":180 , "width": 25, "height": 110,"weight": 20 },
        {"id": 7, "name" : "Motorrad", "length":240 , "width": 100, "height": 150,"weight": 250 },
      ]
    },
    {
      "category": "Möbel",
      formats: [
        {"id": 8, "name" : "verpackte palettierte Möbel", "length":80 , "width": 120, "height": 100,"weight": 100 },
        {"id": 9, "name" : "einzelne unverpackte Möbel", "length":100 , "width": 100, "height": 100,"weight": 50 },
        {"id": 10, "name" : "ganzer Transporter unverpackter Möbel", "length":100 , "width": 100, "height": 100,"weight": 50 },
        {"id": 11, "name" : "kompletter Umzug", "length":100 , "width": 100, "height": 100,"weight": 50 },
      ]
    },
    {
      "category": "Sonstige",
      formats: [
        {"id": 12, "name":"Sonstige / Eigene Maße", "length":80 , "width": 120, "height": 100,"weight": 100 }
      ]
    }
  ]
  constructor( private formBuilder: FormBuilder,  ) {
  }
  
  setValueByFormat = (nextID: number) =>{
    for ( let i = 0; i < this.packPieceFormats.length; i++){
      for (let j = 0 ; j < this.packPieceFormats[i].formats.length; j++){
        if (this.packPieceFormats[i].formats[j].id == nextID){
          let packPieceGroup = this.parentForm.get('packPieceArray') as FormArray
           packPieceGroup.controls[this.index].patchValue({
            amount : 1,
            format: this.packPieceFormats[i].formats[j].id,
            length: this.packPieceFormats[i].formats[j].length,
            width: this.packPieceFormats[i].formats[j].width,
            height: this.packPieceFormats[i].formats[j].height,
            weight: this.packPieceFormats[i].formats[j].weight,
         })
        }
      }
    }
  }

  ngOnInit() {
     this.setValueByFormat(0)
  }
}

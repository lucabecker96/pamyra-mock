import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PackPieceComponent } from './pack-piece.component';

describe('PackPieceComponent', () => {
  let component: PackPieceComponent;
  let fixture: ComponentFixture<PackPieceComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PackPieceComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PackPieceComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

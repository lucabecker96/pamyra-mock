import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {JourneyFormComponent} from './journey-form/journey-form.component'
import {OfferComponent} from './offer/offer.component'

const routes: Routes = [
  { path: 'search', component: JourneyFormComponent },
  { path: 'result', component: OfferComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
 

 }

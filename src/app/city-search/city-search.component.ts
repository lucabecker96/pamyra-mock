import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormControl } from '@angular/forms';
import data from '../../assets/countries.json'
import { HttpClient } from '@angular/common/http';
@Component({
  selector: 'app-city-search',
  templateUrl: './city-search.component.html',
  styleUrls: ['./city-search.component.css']
})

export class CitySearchComponent implements OnInit {
  @Input() parentForm: FormGroup
  @Input() controlName :string
  http: HttpClient
  suggestions= null
  countries = data
  constructor(http: HttpClient) {   
    this.http = http
  }
  autocompleteCity = ():void => {
    
    let toBeSetStringRef =eval("this.parentForm.controls."+this.controlName+"_city.value") // after all, software is just ducktape and strings ;)
    console.log(toBeSetStringRef)
    if(toBeSetStringRef==""){
      console.log('empty textbox, no request')
      this.suggestions = null
    }else{
    this.parentForm.controls.suggestionBase.setValue(toBeSetStringRef)
    this.http.post('http://localhost:4200', this.parentForm.value).subscribe(
      (res) => {
        if(res != null){
        console.log('suggestions',res)
        this.suggestions = res
        }
      }
    )
  }
}
  setSuggestionCountry =(event) :void=> {
    this.parentForm.controls.suggestion_country.setValue( event )
  }
  suggestionClicked =(sugName: string) :void =>{
    // #nichtSchönAberSelten
    let toBeExecutedString ="this.parentForm.controls."+this.controlName+"_city.setValue(sugName)"
    eval( toBeExecutedString )
    this.suggestions = null 
  }
  ngOnInit() {
  }

}

import { Component, OnInit  } from '@angular/core';
import { Router} from '@angular/router'
import {FormGroup, FormBuilder, FormArray, } from "@angular/forms"
import { fbind } from 'q';
import { HttpClient } from '@angular/common/http';
import {PackPieceComponent} from  '../pack-piece/pack-piece.component'

export type FormInterface = {field1: string, field2: string}
@Component({
  selector: 'app-journey-form',
  templateUrl: './journey-form.component.html',
  styleUrls: ['./journey-form.component.css'],
})
export  class JourneyFormComponent implements OnInit {
  result = null;
  journeyForm : FormGroup;
  destination: string;
  packPieces: FormArray;
  http: HttpClient;
  router: Router
  constructor( private formBuilder: FormBuilder, http: HttpClient , router: Router ) {
    this.http =http
    this.router = router
   }
   submitFormValue =() =>{
    this.http.post('https://jsonplaceholder.typicode.com/users',this.journeyForm.value).subscribe((e)=>{
      console.log(e,this.router)
      this.result =e
      this.router.navigate(["./result/",JSON.stringify(e)])
    })
  }
  createPackPiece = (): FormGroup =>{
    return this.formBuilder.group({
      length: [],
      amount: [],
      format: [],
      width: [],
      height: [],
      weight: [],
      stackable: [],
    })
  }
  addPackPiece = ():void => {
    this.packPieces = this.journeyForm.get('packPieceArray') as FormArray
    this.packPieces.push(this.createPackPiece())
    console.log(this.journeyForm)
  }
  ngOnInit() {
    this.journeyForm = this.formBuilder.group({
        suggestionBase:[],
        suggestion_country: ['DE'],
        destination_country: ['DE'],
        destination_city: [''],
        origin_country: ['DE'],
        origin_city: [''],
        customer_type: ['Privat'],
        packPieceArray: this.formBuilder.array([this.createPackPiece()]),  
    })
  }

}

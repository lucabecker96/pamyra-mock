import { Injectable, Injector } from '@angular/core';
import { HttpEvent, HttpHandler, HttpInterceptor, HttpRequest, HttpResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import cities from '../assets/cities.json'
import { Capabilities } from 'protractor';

@Injectable()
export class HttpMockRequestInterceptor implements HttpInterceptor {
    constructor(private injector: Injector) {}
    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {        
        if (request.url === 'http://localhost:4200'){
            console.log('autocomplete',cities)
            console.log(request.body)
            let { suggestion_country, suggestionBase} = request.body
            let suggestions= []
            for (let i = 0; i< cities.length; i++){
                if( cities[i].country == suggestion_country ){
                    if(cities[i].name.toLowerCase().includes(suggestionBase.toLowerCase())){
                        suggestions.push(cities[i])
                        if (suggestions.length > 9){
                            return of(new HttpResponse({ status: 200, body: ((suggestions) as any[]) }));                    
                        } 
                    }
                }
            }
            return of(new HttpResponse({ status: 200, body: ((suggestions) as any[]) }));                   
        }else {
            let offers = [
                {   
                    name : "StaplerKlaus GmbH",
                    tarif : "Staplerfahrttarif",
                    price : 140,
                    payment : "Vorraus",
                    palette : "palettiert",
                    pickUpDate: new Date(),
                    deliveryDate: new Date(),
                }]
        return of(new HttpResponse({ status: 200, body: ((offers) as any[]) }));
        }
    }
}
